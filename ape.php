<?php
require_once("animal.php");

  //buat class Ape inherited dari Animal 
  //contoh yell menggunakan parameter $string echonya langsung di index.php
  class Ape extends Animal {
    public $legs = 2;
    public function yell($string){
        echo "Yell : " . $string;
    }
}

?>