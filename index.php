<?php //release 0
    require_once("animal.php");
    require_once("ape.php");
    require_once("frog.php");
    
    $sheep = new Animal("Shaun");
    echo "Animal Name : " . $sheep->name."<br>";
    echo " Number of Feet : " . $sheep->legs."<br>";
    echo "Cold Blooded: " . $sheep->cold_blooded."<br><br>";
      // release 1
//class Ape
    $sungokong = new Ape("Kera Sakti");
    echo "Animal Name :" . $sungokong->name . "<br>";
    echo " Number of Feet : " . $sungokong->legs."<br>";
    echo "Cold Blooded: " . $sungokong->cold_blooded."<br>";

    echo $sungokong->yell("Auooo") . "<br><br>"; // "Auooo"
//class Frog
    $kodok = new Frog("Buduk");
    echo "Animal Name :" . $kodok->name . "<br>";
    echo " Number of Feet : " . $kodok->legs."<br>";
    echo "Cold Blooded: " . $kodok->cold_blooded."<br>";
    echo $kodok->jump() ; // "hop hop"


?>