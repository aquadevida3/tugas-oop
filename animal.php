<?php
    class Animal {
        public $name;
        public $legs = 4;
        public $cold_blooded = "no";

        public function __construct($string){
            $this->name = $string;
        }
    }
  

// //Release 0
  
// $sheep = new Animal("shaun");

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 4
// echo $sheep->cold_blooded; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>
